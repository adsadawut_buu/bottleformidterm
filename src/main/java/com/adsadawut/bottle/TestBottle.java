/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.bottle;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class TestBottle {
    public static void main(String[] args) {
        Bottle bottle1 = new Bottle(1000); //สร้าง Obj ตัวที่ 1 มีคลาสเป็น Bottle
        Bottle bottle2 = new Bottle(500);  //สร้าง Obj ตัวที่ 2 มีคลาสเป็น Bottle
        System.out.println("Volumn of Bottle1  = "+bottle1.getV()); 
        System.out.println("Volumn of Bottle2  = "+bottle2.getV());
        bottle1.isDrink(100);
        System.out.println("Volumn of Bottle1  = "+bottle1.getV()); 
        bottle1.isDrink(1000);
        System.out.println("Volumn of Bottle1  = "+bottle1.getV()); 
        System.out.println("Volumn of Bottle2  = "+bottle2.reFill(100));
        System.out.println("Volumn of Bottle2  = "+bottle2.reFill(1000));
    }
    
}
