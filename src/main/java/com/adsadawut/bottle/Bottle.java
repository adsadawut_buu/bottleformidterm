/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.bottle;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Bottle {

    private double v;
    
    public Bottle(double v){
        this.v = v;
    }
    
     public double getV() {
        return v;
    }
     
     public double isDrink(double x){
         if(v  >= x){
             v = v - x;
         }else{
             printCantDrink();
         }
         
         return v;
     }
     
     public double reFill(double y){
         v = v + y;
         if(v>= 1000){
             System.out.println("Bottle is Full!!!");
             v = 1000;
         }
         return v;
    }
     
    public void printCantDrink() {
        System.out.println("Can't Drink!!!");
    }
    
}
